/**
 * タブアクションが開いたタイミングでmain.jsから実行される。
 * タブ内の選択文字列を取得し、日付文字列であれば曜日を算出する。
 * 最後にタブアクションに対してリクエストして曜日情報を伝達する。
 */
var DOTW = DOTW || {};
DOTW.WEEK_ARRAY = ["日", "月", "火", "水", "木", "金", "土"];
DOTW.REG_DAY = /^\d{1,2}$/;
DOTW.REG_MONTH = /^\d{1,2}\/\d{1,2}$/;
DOTW.REG_YEAR = /^\d{4}\/\d{1,2}\/\d{1,2}$/;
DOTW.REG_REPLACER = /["日"|"月"|"年"|-]/g;

// @see: http://stackoverflow.com/questions/4818882/js-text-selection-on-gmail
var selectedText = document.getSelection().toString();
if (selectedText == '') {
  var frames = window.frames; 
  for (var i = 0; i < frames.length; i++) {
    if (selectedText == '') { 
      selectedText = frames[i].document.getSelection().toString();
    } else {
      break;
    }
  }
}

var slashSeparated = selectedText.replace(DOTW.REG_REPLACER, '/');
var currentDate = new Date();
var inputYear = currentDate.getFullYear();
var inputMonth = currentDate.getMonth();
var inputDate = currentDate.getDate();

if (DOTW.REG_YEAR.exec(slashSeparated)) {
  var arr = slashSeparated.split('/');
  inputYear = arr[0];
  inputMonth = parseInt(arr[1], 10) - 1;
  inputDate = arr[2];

} else if (DOTW.REG_MONTH.exec(slashSeparated)) {
  var arr = slashSeparated.split('/');
  inputMonth = parseInt(arr[0], 10) - 1;
  inputDate = arr[1];

} else if (DOTW.REG_DAY.exec(slashSeparated)) {
  inputDate = slashSeparated;

} else {
  // エラーとしてタブアクションページに送信
  chrome.runtime.sendMessage({hasError: true}, function(response){});
  return false;
}

var selectedDate = new Date(inputYear, inputMonth, inputDate);
var year = selectedDate.getFullYear();
var month = selectedDate.getMonth() + 1;
var date = selectedDate.getDate();
var dotw = DOTW.WEEK_ARRAY[selectedDate.getDay()];
  
// mainにタブ内処理の結果を通信
chrome.runtime.sendMessage({year: year, month: month, date: date, dotw: dotw}, function(response){});

var callback;

function doProc(_callback) {
	chrome.tabs.getSelected(null, function(tab) {

		chrome.tabs.executeScript(tab.id, {file: "js/exec.js"});

	});
	callback = _callback;
}

// exec.jsからのリクエストをレシーブする
chrome.runtime.onMessage.addListener(function(request) {
	callback(request);
});


$(document).ready(function() {
   
    doProc(function(req)　{

        if (req.hasError) {
            $('#container').append('<p class="message">日付を選択して下さい</p>');  
            return false;
        }

        var year = req.year + '';
        var month = req.month + '';
        var date = req.date + '';
        var dotw = req.dotw;

        var full = year + '/' + month + '/' + date + '(' + dotw + ')';
        var fullZeroPad = year + '/' + ('0' + month).slice(-2) + '/' + ('0' + date).slice(-2) + '(' + dotw + ')';
        var dotwWithBracket = '(' + dotw + ')';

        // リンク作成
        var linkValues = [full, fullZeroPad, dotw, dotwWithBracket];

        linkValues.forEach(function(val) {
          $('#link_list').append('<li><a href="">' + val + '</a></li>');
        });

        $('#link_list a').click(function(){
          copyToClipboard($(this).text());
        });
    });

});

// @see : http://tande.jp/lab/2012/09/1889
function copyToClipboard(txt) {
    var copyArea = $('<textarea/>');
    copyArea.text(txt);
    $('body').append(copyArea);
    copyArea.select();
    document.execCommand('copy');
    copyArea.remove();
}
